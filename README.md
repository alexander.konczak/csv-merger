# CSV Merger

CSV Merger is an easy-to-use command line tool to merge two or more CSV-files (comma separated values) together.
The application will concatenate the entries of the given files and add the header of the first input file.

## Installation
The application comes packaged as a single executable for different platforms.
Just download it from the [releases page](#) and use it on the command line with the commands described in [Usage](#usage).

## Usage
### Linux
```
# sh
./csv-merger OUTPUT.csv INPUT1.csv INPUT2.csv [INPUT3.csv] [...]
```

### Windows
```
# cmd / PowerShell
csv-merger.exe OUTPUT.csv INPUT1.csv INPUT2.csv [INPUT3.csv] [...]
```

## Options
Currently, there are no options except the help display.

### help
Displays information about the application and usage information.
```
csv-merger.exe -h|--help|/h|/help
```

