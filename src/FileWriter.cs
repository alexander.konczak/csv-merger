namespace CSVMerger;
using System.IO;

class CustomFile {
    protected string filePath;
    protected List<string>? lines;
    
    public CustomFile(string filePath) {
        this.filePath = filePath;
    }

    public void SetLines(List<string> lines) {
        this.lines = lines;
    }

    public void Save() {
        if(this.lines == null) {
            throw new Exception("Missing file content");
        }
        StreamWriter file = File.CreateText(this.filePath);
        foreach(string line in this.lines) {
            file.WriteLine(line);
        }
        file.Flush();
        file.Close();
    }
}