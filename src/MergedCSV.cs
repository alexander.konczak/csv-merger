namespace CSVMerger;
class MergedCSV {
    protected string? header;
    protected List<string> entries = new List<string>();
    
    public bool HasHeader() {
        return this.header != null;
    }

    public void SetHeader(string header) {
        this.header = header;
    }

    public void AddEntries(IEnumerable<string> entries) {
        this.entries.AddRange(entries);
    }

    public List<string> ToList() {
        List<string> output = new List<string>();
        if(this.header != null) {
            output.Add(this.header);        
        }
        output.AddRange(this.entries);
        return output;
    }
}
