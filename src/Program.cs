﻿namespace CSVMerger;
using System.IO;
class Application {
    static protected string? outputFilePath;
    static protected string[]? inputFiles;
    static protected MergedCSV output = new MergedCSV();

    static int Main(string[] args) {
        if(args.Length == 0) {
            Application.PrintUsage();
            return 1;
        }
        if(args[0] == "-h" || args[0] == "--help" || args[0] == "/h" || args[0] == "/help") {
            Application.PrintHelp();
            return 0;
        }
        if(args.Length < 2) {
            Console.WriteLine("Fehler: Zu wenig Argumente");
            Application.PrintUsage();
            return 1;
        }
        Application.outputFilePath = args[0];
        Application.inputFiles = new ArraySegment<string>(args, 1, args.Length - 1).ToArray();

        try {
            foreach(string file in Application.inputFiles) {
                Application.ParseFile(file);
            }
        } catch(FileNotFoundException ex) {
            Console.WriteLine(String.Format("Eingabedatei konnte nicht gefunden werden: {0}", ex.FileName));
            return 1;
        }
        
        CustomFile outputFile = new CustomFile(Application.outputFilePath);
        outputFile.SetLines(Application.output.ToList());
        try {
            outputFile.Save();
        } catch(Exception ex) {
            Console.WriteLine(ex);
            return 1;
        }
        return 0;
    }

    static void ParseFile(string fileName) {
        string[] lines = File.ReadAllLines(fileName);
        if(!Application.output.HasHeader()) {
            Application.output.SetHeader(lines[0]);
        }
        ArraySegment<string> entries = new ArraySegment<string>(lines, 1, lines.Length - 1);
        Application.output.AddEntries(entries);
    }

    static void PrintUsage() {
        string appName = System.AppDomain.CurrentDomain.FriendlyName;
        Console.WriteLine(String.Format("Verwendung: {0} AUSGABE EINGABE...", appName));
    }
    
    static void PrintHelp() {
        string appName = System.AppDomain.CurrentDomain.FriendlyName;
        Console.WriteLine(String.Format("{0} ist eine Anwendung zum Zusammenfügen von 2 oder mehr CSV-Dateien gleichen Formates.", appName));
        Application.PrintUsage();
        Console.WriteLine(String.Format("Beispiel: {0} ausgabe.csv datei1.csv datei2.csv datei3.csv", appName));
        Console.WriteLine("Achtung: Die erste Zeile jeder Eingabedatei wird als Header interpretiert und daher nur einmal in die Ausgabedatei geschrieben!");
    }
}
